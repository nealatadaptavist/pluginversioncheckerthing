#!/bin/bash
# Get the latest version from https://bitbucket.org/peterkoczan/pluginversioncheckerthing

INPUTXML=$1
ESC_SEQ="\x1b["
COL_RESET=$ESC_SEQ"39;49;00m"
COL_RED=$ESC_SEQ"31;01m"
COL_GREEN=$ESC_SEQ"32;01m"
COL_YELLOW=$ESC_SEQ"33;01m"
COL_BLUE=$ESC_SEQ"34;01m"
COL_MAGENTA=$ESC_SEQ"35;01m"
COL_CYAN=$ESC_SEQ"36;01m"

WGREEN="{color:green}"
WYELLOW="{color:orange}"
WRED="{color:red}"
WCX="{color}"

function paginate { # First parameter is the URL to paginate the second is the final output filename
        curl -s $1"?offset=0&limit=50" > /tmp/$2.0
        NEXTPAGE=`jq "._links.next[]?" /tmp/$2.0`
        VERPAGE=1
        PAGEOFFSET=50
        while [ -n "$NEXTPAGE" ]; do
                curl -s $1"?offset="$PAGEOFFSET"&limit=50" > /tmp/$2.$VERPAGE
                NEXTPAGE=`jq "._links.next[]?" /tmp/$2.$VERPAGE`
                VERPAGE=$((VERPAGE + 1))
                PAGEOFFSET=$((PAGEOFFSET + 50))
        done

        cat /tmp/$2.* > /tmp/$2
        rm -f /tmp/$2.*
}
BUNDLEDPLUGINS="(com.atlassian.jirafisheyeplugin|com.atlassian.jira.plugin.ext.bamboo|rome.rome-1.0|com.atlassian.oauth.consumer.sal|com.atlassian.auiplugin|com.atlassian.bitbucket.server.bitbucket-ref-restriction|com.atlassian.bitbucket.server.bitbucket-velocity-helper|com.atlassian.bitbucket.server.bitbucket-markup-renderers|com.atlassian.activeobjects.confluence.spi|com.atlassian.activeobjects.activeobjects-plugin|confluence.sections.admin.header|confluence.sections.admin|confluence.sections.admin.tasks|confluence.macros.advanced|com.atlassian.analytics.analytics-client|com.atlassian.analytics.analytics-whitelist|org.apache.servicemix.bundles.spring-jdbc-4.2.0.RELEASE_2|org.apache.servicemix.bundles.spring-tx-4.2.0.RELEASE_2|com.atlassian.applinks.applinks-basicauth-plugin|com.atlassian.applinks.applinks-plugin|com.atlassian.applinks.applinks-cors-plugin|com.atlassian.applinks.applinks-oauth-plugin|com.atlassian.applinks.applinks-trustedapps-plugin|com.atlassian.labs.atlassian-bot-killer|com.atlassian.plugins.browser.metrics.browser-metrics-plugin|com.atlassian.cluster.monitoring.cluster-monitoring-plugin|com.atlassian.plugins.editor|com.atlassian.crowd.embedded.admin|com.atlassian.atlassian-failure-cache-plugin|com.atlassian.gadgets.atlassian-gadgets-api-3.3.24|com.atlassian.gadgets.oauth.serviceprovider|atlassian-gadgets-shared-3.3.24.jar|com.atlassian.gadgets.atlassian-gadgets-spi-3.3.24|com.atlassian.healthcheck.atlassian-healthcheck|com.atlassian.plugins.atlassian-help-tips|com.atlassian.plugins.base-hipchat-integration-plugin|com.atlassian.plugins.base-hipchat-integration-plugin-api|com.atlassian.httpclient.atlassian-httpclient-plugin|com.atlassian.jwt.jwt-plugin|com.atlassian.plugins.less-transformer-plugin|com.atlassian.plugins.atlassian-nav-links-plugin|com.atlassian.plugins.atlassian-nps-plugin|com.atlassian.plugin.notifications.notifications-module|com.atlassian.oauth.admin|com.atlassian.oauth.atlassian-oauth-api-2.0.2|com.atlassian.oauth.consumer|com.atlassian.oauth.consumer.sal|com.atlassian.oauth.atlassian-oauth-consumer-spi|com.atlassian.oauth.serviceprovider|com.atlassian.oauth.serviceprovider.sal|com.atlassian.oauth.atlassian-oauth-service-provider-spi|com.atlassian.plugins.avatar|com.atlassian.plugin.jslibs|com.atlassian.plugins.atlassian-plugins-webresource-plugin|com.atlassian.plugins.atlassian-plugins-webresource-rest|com.atlassian.prettyurls.atlassian-pretty-urls-plugin|com.atlassian.plugins.rest.atlassian-rest-module|com.atlassian.soy.soy-template-plugin|com.atlassian.plugin.atlassian-spring-scanner-annotation|com.atlassian.plugin.atlassian-spring-scanner-runtime|com.atlassian.templaterenderer.api|com.atlassian.templaterenderer.atlassian-template-renderer-velocity1.6-plugin|com.atlassian.confluence.plugins.templates-framework|com.atlassian.plugins.tinymce|com.atlassian.auiplugin|com.atlassian.upm.plugin-license-storage-plugin|com.atlassian.webhooks.atlassian-webhooks-plugin|com.atlassian.plugins.atlassian-whitelist-core-plugin|com.atlassian.plugins.atlassian-whitelist-ui-plugin|com.atlassian.plugins.atlassian-xhtml|confluence.sections.attachments|com.atlassian.confluence.plugins.attachmentExtractors|confluence.macros.basic|confluence.sections.browse|confluence.extra.chart|com.atlassian.confluence.ext.newcode-macro-plugin|confluence.comment.action|com.atlassian.activeobjects.confluence.spi|com.atlassian.streams.confluence|com.atlassian.streams.confluence.inlineactions|confluence.analytics.whitelist|com.atlassian.confluence.plugins.confluence-jira-content|confluence.extra.attachments|confluence.aui.staging|com.atlassian.confluence.plugins.confluence-avatar-plugin|com.atlassian.confluence.rpc.confluence-axis-soap-plugin|com.atlassian.confluence.plugins.confluence-notifications-batch-plugin|com.atlassian.confluence.plugins.xmlrpc.bloggingrpc|com.atlassian.confluence.plugins.editor-height-fix|com.atlassian.confluence.plugins.confluence-browser-metrics|com.atlassian.confluence.plugins.confluence-business-blueprints|com.atlassian.confluence.plugins.confluence-cache-management-plugin|com.atlassian.confluence.plugins.confluence-collaborative-editor-plugin|com.atlassian.confluence.plugins.confluence-content-notifications-plugin|com.atlassian.confluence.plugins.confluence-content-property-storage|com.atlassian.confluence.plugins.confluence-content-report-plugin|com.atlassian.confluence.contributors|confluence.macros.core|confluence.macros.multimedia|com.atlassian.querylang.confluence-cql-plugin|com.atlassian.confluence.plugins.confluence-create-content-plugin|com.atlassian.favicon.confluence-custom-favicon-plugin|com.atlassian.confluence.plugins.confluence-daily-summary-email|com.atlassian.confluence.plugins.confluence-darkfeatures-rest|com.atlassian.confluence.plugins.dashboard-actions|com.atlassian.confluence.plugins.confluence-dashboard|confluence.help.paths|com.atlassian.confluence.plugins.confluence-default-space-content-plugin|com.atlassian.confluence.plugins.confluence-document-conversion-library|com.atlassian.confluence.plugins.drag-and-drop|com.atlassian.confluence.plugins.confluence-eap-feature-switch|com.atlassian.confluence.plugins.confluence-easyuser-admin|com.atlassian.confluence.plugins.confluence-edge-index|com.atlassian.confluence.tinymceplugin|com.atlassian.confluence.plugins.editor-loader|com.atlassian.confluence.editor|com.atlassian.confluence.plugins.confluence-email-gateway|com.atlassian.confluence.plugins.confluence-email-resources|com.atlassian.confluence.plugins.confluence-email-to-page|com.atlassian.confluence.plugins.confluence-email-tracker|ac.confluence.languages.en_US|com.atlassian.confluence.plugins.expand-macro|com.atlassian.confluence.plugins.confluence-extractor-api-plugin|com.atlassian.confluence.plugins.confluence-feature-discovery-plugin|com.atlassian.confluence.plugins.confluence-file-notifications|com.atlassian.confluence.plugins.confluence-files|com.atlassian.confluence.plugins.confluence-fixed-headers|tac.confluence.languages.fr_FR|tac.confluence.languages.de_DE|confluence.getting-started|com.atlassian.confluence.plugins.confluence-hipchat-emoticons-plugin|com.atlassian.confluence.plugins.confluence-hipchat-integration-plugin|confluence.macros.html|com.atlassian.confluence.plugins.confluence-image-attributes|com.atlassian.confluence.plugins.confluence-inline-comments|com.atlassian.confluence.plugins.confluence-inline-tasks|tac.confluence.languages.ja_JP|com.atlassian.confluence.plugins.confluence-jira-metadata|confluence.extra.jira|com.atlassian.confluence.rpc.confluence-json-rpc-plugin|com.atlassian.confluence.keyboardshortcuts|com.atlassian.confluence.plugins.confluence-knowledge-base|tac.confluence.languages.ko_KR|com.atlassian.confluence.plugins.confluence-labels|com.atlassian.confluence.plugins.confluence-license-banner|com.atlassian.confluence.plugins.confluence-like|com.atlassian.confluence.plugins.confluence-link-browser|confluence.extra.livesearch|com.atlassian.confluence.plugins.confluence-lookandfeel|com.atlassian.confluence.plugins.confluence-macro-browser|com.atlassian.confluence.plugins.confluence-macro-usage|com.atlassian.confluence.plugins.confluence-mail-archiving|com.atlassian.confluence.plugins.confluence-mentions-plugin|com.atlassian.confluence.plugins.confluence-mobile|com.atlassian.confluence.plugins.confluence-monitoring|com.atlassian.confluence.plugins.confluence-monitoring-console|com.atlassian.confluence.plugins.confluence-nav-links|com.atlassian.confluence.plugins.confluence-notifications-spi-plugin|com.atlassian.confluence.restplugin|com.atlassian.confluence.plugins.confluence-onboarding|com.atlassian.confluence.plugins.confluence-page-banner|com.atlassian.confluence.plugins.confluence-page-layout|com.atlassian.confluence.plugins.confluence-paste|com.atlassian.confluence.extra.flyingpdf|tac.confluence.languages.pt_BR|com.atlassian.confluence.plugins.confluence-previews|com.atlassian.confluence.plugins.quickedit|com.atlassian.confluence.plugins.confluence-quicknav|com.atlassian.confluence.plugins.quickreload|com.atlassian.confluence.plugins.reliablesave|confluence.extra.confluencerpc|com.atlassian.confluence.plugins.confluence-remote-page-view-plugin|com.atlassian.confluence.plugins.confluence-reply-to-email|com.atlassian.confluence.plugins.confluence-request-access-plugin|com.atlassian.confluence.plugins.confluence-required-health-checks|com.atlassian.confluence.plugins.confluence-rest-resources|com.atlassian.confluence.plugins.confluence-rest-navigation|com.atlassian.confluence.plugins.confluence-page-restrictions-dialog|com.atlassian.confluence.plugins.confluence-rest-jersey|com.atlassian.confluence.plugins.confluence-roadmap-plugin|tac.confluence.languages.ru_RU|com.atlassian.confluence.plugins.confluence-sal-plugin|com.atlassian.confluence.plugins.confluence-schedule-admin|com.atlassian.confluence.plugins.search.confluence-search|com.atlassian.confluence.plugins.confluence-software-blueprints|com.atlassian.confluence.plugins.confluence-software-project|com.atlassian.confluence.plugins.confluence-sortable-tables|com.atlassian.confluence.plugins.soy|com.atlassian.confluence.plugins.confluence-space-blueprints|com.atlassian.confluence.plugins.confluence-space-directory|com.atlassian.confluence.plugins.confluence-space-ia|com.atlassian.confluence.plugins.confluence-spaces|tac.confluence.languages.es_ES|com.atlassian.confluence.plugins.confluence-templates|com.atlassian.confluence.plugins.confluence-ui-components|com.atlassian.confluence.plugins.confluence-ui-rest|com.atlassian.confluence.ext.usage|com.atlassian.confluence.plugins.confluence-user-profile|com.atlassian.confluence.plugins.confluence-user-rest|com.atlassian.confluence.plugins.confluence-view-file-macro|com.atlassian.confluence.plugins.view-source|com.atlassian.confluence.plugins.view-storage-format|com.atlassian.confluence.plugins.watch-button|com.atlassian.confluence.plugins.whatsnew|confluence.content.action.menu|confluence.sections.page.temp|confluence.extractors.core|confluence.filters.core|confluence.listeners.core|confluence.converters.core|confluence.lifecycle.core|confluence.sections.create|crowd.system.passwordencoders|confluence.macros.dashboard|confluence.macro.metadata.provider|confluence.search.contentname|com.atlassian.confluence.themes.default|com.atlassian.confluence.plugins.dialog-wizard|confluence.sections.admin.indexing|com.atlassian.confluence.plugins.doctheme|com.atlassian.plugins.document-conversion-library|confluence.extra.dynamictasklist2|confluence.editor.actions|confluence.sections.settings.edit|com.atlassian.gadgets.embedded|confluence.sections.user.follow.list|com.atlassian.gadgets.directory|com.atlassian.gadgets.publisher|com.atlassian.confluence.plugins.gadgets|com.atlassian.confluence.plugins.gadgets.spi|confluence.header.imagelinks|confluence.sections.help|com.atlassian.bundles.icu4j-3.8.0.1|com.atlassian.confluence.image.effects.ImageEffectsPlugin|com.atlassian.plugins.imageio-enabler-0.0.0|confluence.extra.impresence2|confluence.extra.information|com.atlassian.plugins.issue-status-plugin|com.springsource.net.jcip.annotations-1.0.0|com.atlassian.integration.jira.jira-integration-plugin|com.atlassian.plugins.jquery|com.atlassian.bundles.json-20070829.0.0.1|com.atlassian.plugins.shortcuts.atlassian-shortcuts-plugin|confluence.extra.layout|com.atlassian.confluence.plugins.confluence-license-rest|confluence.search.lucene.boosting|com.atlassian.labs.lucene-compat-plugin|confluence.search.lucene.initialisation|com.atlassian.confluence.plugins.confluence-macro-usage-admin-plugin|confluence.sections.news|com.atlassian.confluence.extra.officeconnector|com.atlassian.gadgets.opensocial|com.atlassian.confluence.plugins.confluence-highlight-actions|confluence.sections.page.operations|confluence.extra.masterdetail|confluence.search.mappers.lucene|confluence.sections.page|com.atlassian.confluence.plugins.pagetree|confluence.macros.profile|com.atlassian.confluence.plugins.profile-picture|confluence.sections.profile|com.atlassian.plugins.atlassian-project-creation-plugin|com.atlassian.plugins.confluence-project-creation|com.atlassian.confluence.plugins.recently-viewed-plugin|confluence.sections.admin.generalconfig|confluence.sections.search.view|com.atlassian.confluence.plugins.share-page|confluence.sections.space.admin|confluence.sections.space.advanced|confluence.sections.space.browse|confluence.sections.space.export.view|confluence.sections.space|confluence.sections.space.pages|confluence.sections.space.tools|com.atlassian.plugins.atlassian-spring-interceptor-adapter-plugin-1.1|com.atlassian.confluence.plugins.status-macro|com.atlassian.confluence.plugins.sticky-table-headers|com.atlassian.streams.streams-api|com.atlassian.streams.core|com.atlassian.streams.actions|com.atlassian.streams|com.atlassian.streams.streams-spi|com.atlassian.support.healthcheck.support-healthcheck-plugin|com.atlassian.confluence.plugins.system-templates|confluence.web.components|confluence.web.panel.renderers|confluence.web.panels|confluence.web.resources|org.randombits.confluence.toc|com.atlassian.plugins.confluence-tdm-merger|confluence.sections.admin.upgrade|confluence.admin.user|confluence.user.hover.menu|confluence.extra.userlister|confluence.user.menu|confluence.user.menu.concise|confluence.sections.user.view.follow|confluence.sections.profile.view|confluence.extra.webdav|com.atlassian.confluence.extra.widgetconnector|confluence.xhtml.wikimarkup|confluence.renderer.components|com.atlassian.mywork.mywork-common-plugin|com.atlassian.mywork.mywork-confluence-provider-plugin|com.atlassian.mywork.mywork-confluence-host-plugin)"


ERR=0
if ! which jq >/dev/null; then echo -e "$COL_RED You do not have \"jq\" installed (or not found in PATH).$COL_RESET"; ERR=1;
fi

if ! which curl >/dev/null; then echo -e "$COL_RED You do not have \"curl\" installed (or not found in PATH).$COL_RESET"; ERR=1;
fi

if ! which xmlstarlet >/dev/null; then printf "$COL_RED You do not have \"xmlstarlet\" installed (or not found in PATH).$COL_RESET"; ERR=1;
fi

if ! which egrep >/dev/null; then echo -e "$COL_RED You do not have \"egrep\" installed (or not found in PATH).$COL_RESET"; ERR=1;
fi

if ! mkdir -p /var/tmp/pvct; then echo -e "$COL_RED Could not create cache directory under /var/tmp/pvct!$COL_RESET"; ERR=1;
fi

if [ ! $# -gt 0 ]; then echo -e $COL_MAGENTA"Plugin"$COL_RED"Version"$COL_GREEN"Checker"$COL_BLUE"Thing"$COL_RESET" http://bitbucket.org/peterkoczan/pluginversioncheckerthing/\n\n"$COL_YELLOW"Usage:\n\t$0 <path-to>/application.xml (must be the first parameter!)\n\t\t[-i (outputs incompatible plugins only)]\n\t\t[-n Hide plugins with no Marketplace information]\n\t\t[-v <version> will override the version from application.xml with a custom value, optional]\n\t\t[-b will perform the check for bundled plugins as well]\n\t\t[-w will produce the output in wiki-markup]\n\t\t[-r will reset the local json cache (use this if you suspect false results)]\n\t\t[-dc will perform Data Center compatibility check]\n\n\t\tTip: use \"pbcopy\" under MacOS and \"xclip -selection clipboard\" under Linux to get the command output to your clipboard. (Will not work in Docker)$COL_RESET"; ERR=1;
fi

if [ "$ERR" = "1" ]; then exit 1;
fi

touch /var/tmp/pvct/placeholder

for dfile in `grep -l "<\!DOCTYPE html>" /var/tmp/pvct/*`; do
	rm $dfile;
done

find /var/tmp/pvct/ -name "*fullcache.json" -type f -mtime +1 -delete # need to get rid of stale entries in the cache
find /var/tmp/pvct/ -name "*-cache.json" -type f -mtime +5 -delete

if [ -d ./.cache ]; then
	cp -r ./.cache/*noinfo* /var/tmp/pvct/
fi

VEROVERRIDE=false;
WIKIOUTPUT=false;
DCCHECK=false;
HIDENOINFO=false;
INCOMPATIBLEONLY=false;
CHECKBUNDLED=false;

if [ ! -s /var/tmp/pvct/cachereset ]; then
	rm -rf /var/tmp/pvct/*.json; echo "Cache has been reset (post-upgrade cleanup, ran only once)";
	date > /var/tmp/pvct/cachereset;
fi

while [ "${1+defined}" ]; do
	if [[ "$1" == "-i" ]]; then
		INCOMPATIBLEONLY=true;
	elif [[ "$1" == "-v" ]]; then
		VEROVERRIDE=true
	elif [[ "$1" == "-w" ]]; then
		WIKIOUTPUT=true;
	elif [[ "$1" == "-r" ]]; then
		rm -rf /var/tmp/pvct/*.json; echo "Cache has been reset"; exit 0;
	elif [[ "$1" == "-dc" ]]; then
		DCCHECK=true;
	elif [[ "$1" == "-n" ]]; then
		HIDENOINFO=true;
	elif [[ "$1" == "-b" ]]; then
		CHECKBUNDLED=true;
        elif [[ "$1" == "-i" ]]; then
		INCOMPATIBLEONLY=true;
	elif [ $VEROVERRIDE = true ]; then
		OVERSION=$1
		VEROVERRIDE=false	
	fi

	shift
done

if [ $INCOMPATIBLEONLY != true ]; then
	INCOMPATIBLEONLY=false;
fi

if [ $WIKIOUTPUT != true ]; then
	WIKIOUTPUT=false;
fi

if [ -f "$INPUTXML" ]; then
	        egrep -v "(ARCHIVED\_COLLECTORS|com\.akelesconsulting\.jira\.plugins\.GaugeConfigResource|wallboardPluginSettings|org\.apache\.activemq\.util\.LockFile|^\#|java\.util\.prefs|com\.atlassian\.plugins\.tutorial\.xproductadminui|kahadb\.util|chaperone-sd-gettingstarted|<=|com\.atlassian\.upm\.UserSettingsStore-acceptedMpac|java-util-prefs-jql|chaperone-sd|SQLFeed-JV|\/net\/customware\/jira\/utils\/license\/\/license|53e11833cbafc0a01dfb716c60d8cf90|java-util-prefs-\/\/\/Exocet|BASH_FUNC_mc|com\.akelesconsulting\.jira\.plugins\.rest\.LicenseConfigResource|bannerHiddenTillDay|Limite-d\&amp\;apos\;utilisateurs|Date-de-fin-de-la-p\&amp\;233\;riode-de-maintenance|Date-d\&amp\;apos\;achat|Num\&amp\;233\;ro-d\&amp\;apos\;\&amp\;233\;ligibilit\&amp\;233\;-d\&amp\;apos\;assistance|<eazybi\.database>|com\.bit\.storage\.storymap\/maps|com\.workday\..*onfigResource|com\.adobe\.jira\.plugins\.v1sync\.mappings|595\.cfgdir|pubmatic\.comstudio\.haup\.message\.warn|jira\.plugin\.jqllimiter\.config|at\.celix\.jira\.plugins\..*\.configuration\.ConfigResource|LESS_TERMCAP_??|field-appearance-config|tasks-user-pleblanc)" "$INPUTXML" >/tmp/application-properties.xml
	ORIGINPUTXML=$INPUTXML
        INPUTXML="/tmp/application-properties.xml"
        if ! xmlstarlet val -q $INPUTXML; then
		echo -e $COL_RED" The specified input file $ORIGINPUTXML is not a valid XML document.$COL_RESET"
		exit 1
	fi

	PRODUCT=`xmlstarlet sel -t -m '//product' -v "@name" $INPUTXML`
	if [[ "$OVERSION" == "" ]]; then
        	PRODUCTVERSION=`xmlstarlet sel -t -m "//product/@version" -v . $INPUTXML`
	else
		PRODUCTVERSION=$OVERSION;
	fi

	if [ "$PRODUCT" = "Confluence" ]; then
		PLUGINCMD=`xmlstarlet sel -t -m '//enabled-plugins/plugin[bundled="User installed" or bundled="User installed?" or bundled="Vom Benutzer installiert" or contains(bundled,"utilisateur") or bundled="Instalado por el usuario" or contains(bundled,";?")]' -v key -o "," -v version -n $INPUTXML`
	elif [ "$PRODUCT" = "Stash" ]; then
		PLUGINCMD=`xmlstarlet sel -t -m '//plugins/plugin[status="ENABLED"]' -v key -o "," -v version -n $INPUTXML`
        elif [ "$PRODUCT" = "Bitbucket" ]; then
                PLUGINCMD=`xmlstarlet sel -t -m '//plugins/plugin[status="ENABLED"]' -v key -o "," -v version -n $INPUTXML`
	elif [ "$PRODUCT" = "JIRA" ]; then
                PLUGINCMD=`xmlstarlet sel -t -m '//enabled-plugins/plugin[bundled="User installed" or bundled="Vom Benutzer installiert" or bundled="Instalado por el usuario" or contains(bundled,"utilisateur") or contains(bundled,";?")]' -v key -o "," -v version -n $INPUTXML`
	elif [ "$PRODUCT" = "FishEye" ]; then
                PLUGINCMD=`xmlstarlet sel -t -m '//enabled-plugins/plugin' -v key -o "," -v version -n $INPUTXML`
	elif [ "$PRODUCT" = "Crucible" ]; then
	        PLUGINCMD=`xmlstarlet sel -t -m '//enabled-plugins/plugin' -v key -o "," -v version -n $INPUTXML`
	elif [ "$PRODUCT" = "Bamboo" ]; then
		PLUGINCMD=`xmlstarlet sel -t -m '//plugins/plugin[bundled="User installed" or bundled="Vom Benutzer installiert" or contains(bundled,"utilisateur")]' -v key -o "," -v version -n $INPUTXML`
	else
		echo -e "$COL_RED Product not yet supported. $COL_RESET"
		exit 1
	fi

	if [ $CHECKBUNDLED = false ]; then
		PLUGINCMD=`echo "$PLUGINCMD" |egrep -v "$BUNDLEDPLUGINS"`
	fi
	
	if [ "$PRODUCT" = "Stash" ]; then PRODUCT="Bitbucket"; fi

	PRODUCTLOWER=`echo $PRODUCT|tr "[A-Z]" "[a-z]"`

	paginate "https://marketplace.atlassian.com/rest/2/applications/"$PRODUCTLOWER"/versions" versions.json

        PRODUCTBUILDNUMBER=`jq "._embedded.versions[] | select(.version == \"$PRODUCTVERSION\").buildNumber" /tmp/versions.json`

	rm -f /tmp/versions.json

	if [ $WIKIOUTPUT = true ]; then
		echo "h5. Plugin compatibility results for $PRODUCT $PRODUCTVERSION (build number: $PRODUCTBUILDNUMBER)"
		if [ $DCCHECK = true ]; then
	                echo "||Plugin key||Plugin version||Compatible?||Live plugin?||DC compatible?||#/Total||"
		else	 
			echo "||Plugin key||Plugin version||Compatible?||Live plugin?||#/Total||"
		fi
	else
		echo -e $COL_BLUE"Checking plugin compatibility for $PRODUCT $PRODUCTVERSION (build number: $PRODUCTBUILDNUMBER)... $COL_RESET"
	fi
	
	TOTALPLUGINS=`echo "$PLUGINCMD"|wc -l|tr -d "\n"`
	COUNTPLUGINS=0;
	for plugin in $PLUGINCMD; do
		COUNTPLUGINS=$((COUNTPLUGINS + 1))
		PLUGINKEY=`echo $plugin|cut -f1 -d","`
		#VERSION=`echo $plugin|cut -f2 -d","|cut -f1 -d"-"`
                VERSION=`echo $plugin|cut -f2 -d","`
		if [ -s /var/tmp/pvct/$PLUGINKEY-$VERSION-noinfo.json ]; then #checking if we've been returned no info for this plugin earlier
			if [[ "$HIDENOINFO" == "false" && "$INCOMPATIBLEONLY" == "false" ]]; then
                		if [ $WIKIOUTPUT = true ]; then
					if [ $DCCHECK = true ]; then
						echo "|"$WYELLOW$PLUGINKEY$WCX"|"$VERSION"|(?)|(?)|(?)|"$COUNTPLUGINS"/"$TOTALPLUGINS"|"
					else
                        			echo "|"$WYELLOW$PLUGINKEY$WCX"|"$VERSION"|(?)|(?)|"$COUNTPLUGINS"/"$TOTALPLUGINS"|"
					fi
                        	else
                               		echo -e "$COL_YELLOW [?] $PLUGINKEY (version: $VERSION) has no info on marketplace (possibly system or custom plugin). ("$COUNTPLUGINS"/"$TOTALPLUGINS")"$COL_RESET
                        	fi
			fi
			continue
		fi

		if [[ ! -s /var/tmp/pvct/$PLUGINKEY-$VERSION-cache.json && ! -s /var/tmp/pvct/$PLUGINKEY-fullcache.json ]]; then 
			#curl -s "https://marketplace.atlassian.com/rest/1.0/plugins/"$PLUGINKEY"/version/"$VERSION -o /var/tmp/pvct/$PLUGINKEY-$VERSION-cache.json
			paginate "https://marketplace.atlassian.com/rest/2/addons/"$PLUGINKEY"/versions" $PLUGINKEY.versions
			PURI=`jq -r "._embedded.versions[]  | select(.name == \"$VERSION\")._links.self.href" /tmp/$PLUGINKEY.versions`
			if [[ "$PURI" == "" ]]; then
				echo "noinfo" > /var/tmp/pvct/$PLUGINKEY-$VERSION-noinfo.json
			else
				curl -s https://marketplace.atlassian.com$PURI -o /var/tmp/pvct/$PLUGINKEY-$VERSION-cache.json
			fi
			
			
		fi

		if [ -s /var/tmp/pvct/$PLUGINKEY-$VERSION-cache.json ]; then
			DCRESULT=""; DCRESULTW="";
                        # MAXBUILD=`jq ".version.compatibilities[] | select(.applicationName == \"$PRODUCT\").max.buildNumber" /var/tmp/pvct/$PLUGINKEY-$VERSION-cache.json`
                        # MINBUILD=`jq ".version.compatibilities[] | select(.applicationName == \"$PRODUCT\").min.buildNumber" /var/tmp/pvct/$PLUGINKEY-$VERSION-cache.json`
			# DCCOMPATIBLE=`jq ".version.deployment.dataCenterCompatible" /var/tmp/pvct/$PLUGINKEY-$VERSION-cache.json`
			MINBUILD=`jq ".compatibilities[] | select(.application == \"$PRODUCTLOWER\").hosting.server.min.build" /var/tmp/pvct/$PLUGINKEY-$VERSION-cache.json`
                        MAXBUILD=`jq ".compatibilities[] | select(.application == \"$PRODUCTLOWER\").hosting.server.max.build" /var/tmp/pvct/$PLUGINKEY-$VERSION-cache.json`
			DCCOMPATIBLE=`jq ".deployment.dataCenter" /var/tmp/pvct/$PLUGINKEY-$VERSION-cache.json`

			if [ $DCCHECK = true ]; then
				if [ $DCCOMPATIBLE = true ]; then
					DCRESULTW="|(/)"
					DCRESULT="[DC: Yes] "
				else
					DCRESULTW="|(x)"
					DCRESULT="[DC: No] "
				fi
			else
				DCRESULTW=""
				DCRESULT=""
				
			fi

			if ((PRODUCTBUILDNUMBER >= MINBUILD && PRODUCTBUILDNUMBER <= MAXBUILD)); then
				if [ $INCOMPATIBLEONLY = false ]; then
					if [ $WIKIOUTPUT = true ]; then
						echo "|"$WGREEN$PLUGINKEY" ([link|https://marketplace.atlassian.com/plugins/"$PLUGINKEY"/versions])"$WCX"|"$VERSION"|(/)|(/)"$DCRESULTW"|"$COUNTPLUGINS"/"$TOTALPLUGINS"|"
					else
                                		echo -e "$COL_GREEN [+] $PLUGINKEY (version: $VERSION) is marked as compatible with $PRODUCT $PRODUCTVERSION $DCRESULT("$COUNTPLUGINS"/"$TOTALPLUGINS")"$COL_RESET
					fi
				fi
			else
				if [ $WIKIOUTPUT = true ]; then
					echo "|"$WRED$PLUGINKEY" ([link|https://marketplace.atlassian.com/plugins/"$PLUGINKEY"/versions])"$WCX"|"$VERSION"|(x)|(/)"$DCRESULTW"|"$COUNTPLUGINS"/"$TOTALPLUGINS"|"
				else
					echo -e "$COL_RED [!] $PLUGINKEY (version: $VERSION) is marked as INCOMPATIBLE with $PRODUCT $PRODUCTVERSION $DCRESULT("$COUNTPLUGINS"/"$TOTALPLUGINS")"$COL_RESET
				fi
			fi
		else
			if [ $INCOMPATIBLEONLY = false ]; then
				if [[ ! -s /var/tmp/pvct/$PLUGINKEY-fullcache.json && ! -s /var/tmp/pvct/$PLUGINKEY-$VERSION-archived-cache.json ]]; then
					curl -s "https://marketplace.atlassian.com/rest/1.0/plugins/archive/"$PLUGINKEY -o /var/tmp/pvct/$PLUGINKEY-$VERSION-archived-cache.json
				fi

				if [[ -s /var/tmp/pvct/$PLUGINKEY-$VERSION-archived-cache.json ]]; then
					DCRESULT=""; DCRESULTW="";
					MAXBUILD=`jq ".version.compatibilities[] | select(.applicationName == \"$PRODUCT\").max.buildNumber" /var/tmp/pvct/$PLUGINKEY-$VERSION-archived-cache.json`
                        		MINBUILD=`jq ".version.compatibilities[] | select(.applicationName == \"$PRODUCT\").min.buildNumber" /var/tmp/pvct/$PLUGINKEY-$VERSION-archived-cache.json`
					DCCOMPATIBLE=`jq ".version.deployment.dataCenterCompatible" /var/tmp/pvct/$PLUGINKEY-$VERSION-archived-cache.json`
                        		if [ $DCCHECK = true ]; then
                                		if [ $DCCOMPATIBLE = true ]; then
                                        		DCRESULTW="|(/)"
                                        		DCRESULT="[DC: Yes] "
                                		else
                                        		DCRESULTW="|(x)"
                                        		DCRESULT="[DC: No] "
                                		fi
                        		else
                                		DCRESULTW=""
                                		DCRESULT=""

				fi

                        		if ((PRODUCTBUILDNUMBER >= MINBUILD && PRODUCTBUILDNUMBER <= MAXBUILD)); then
						if [ $WIKIOUTPUT = true ]; then
							echo "|"$WGREEN$PLUGINKEY" ([link|https://marketplace.atlassian.com/archive/"$PLUGINKEY"])"$WCX"|"$VERSION"|(/)|(x)"$DCRESULTW"|"$COUNTPLUGINS"/"$TOTALPLUGINS"|"
						else
							echo -e "$COL_GREEN [+] $PLUGINKEY (version: $VERSION) is marked compatible with $PRODUCT $PRODUCTVERSION "$COL_YELLOW"but is an archived plugin! $DCRESULT("$COUNTPLUGINS"/"$TOTALPLUGINS")"$COL_RESET
						fi
					else
						if [ $WIKIOUTPUT = true ]; then 
							echo "|"$WRED$PLUGINKEY" ([link|https://marketplace.atlassian.com/archive/"$PLUGINKEY"])"$WCX"|"$VERSION"|(x)|(x)"$DCRESULTW"|"$COUNTPLUGINS"/"$TOTALPLUGINS"|"
						else
                                                	echo -e "$COL_RED [!] $PLUGINKEY (version: $VERSION) is marked incompatible with $PRODUCT $PRODUCTVERSION and is an archived plugin! $DCRESULT("$COUNTPLUGINS"/"$TOTALPLUGINS")"$COL_RESET
						fi
					fi

				else
					if [ ! -s /var/tmp/pvct/$PLUGINKEY-fullcache.json ]; then
						curl -s "https://marketplace.atlassian.com/rest/1.0/plugins/"$PLUGINKEY -o /var/tmp/pvct/$PLUGINKEY-fullcache.json # if this exists, then it's only the particular version that has no info, not the whole plugin
					fi

					if [ -s /var/tmp/pvct/$PLUGINKEY-fullcache.json ]; then
						if [ $WIKIOUTPUT = true ]; then
							echo "|"$WRED$PLUGINKEY$WCX" ([link|https://marketplace.atlassian.com/plugins/$PLUGINKEY/versions])|"$VERSION"|(x)|(?) $DCRESULTW|"$COUNTPLUGINS"/"$TOTALPLUGINS"|"
						else
							echo -e "$COL_RED [?] $PLUGINKEY (version: $VERSION) has no info on marketplace but there are other versions! ("$COUNTPLUGINS"/"$TOTALPLUGINS")"$COL_RESET
						fi
					else
						if [ $HIDENOINFO = false ]; then
                                                	if [ $WIKIOUTPUT = true ]; then
                                                        	echo "|"$WYELLOW$PLUGINKEY$WCX"|"$VERSION"|(?)|(?)"$DCRESULTW"|"$COUNTPLUGINS"/"$TOTALPLUGINS"|"
                                                	else
                                                        	echo -e "$COL_YELLOW [?] $PLUGINKEY (version: $VERSION) has no info on marketplace (possibly system or custom plugin). ("$COUNTPLUGINS"/"$TOTALPLUGINS")"$COL_RESET
                                                	fi
						fi

						echo "noinfo" > /var/tmp/pvct/$PLUGINKEY-$VERSION-noinfo.json
					fi
				fi
			else
                                        if ((PRODUCTBUILDNUMBER >= MINBUILD && PRODUCTBUILDNUMBER <= MAXBUILD)); then
                                            a=true;
					else
						if [ $WIKIOUTPUT = true ]; then 
							echo "|"$WRED$PLUGINKEY" ([link|https://marketplace.atlassian.com/archive/"$PLUGINKEY"])"$WCX"|"$VERSION"|(x)|(x)"$DCRESULTW"|"$COUNTPLUGINS"/"$TOTALPLUGINS"|"
						else
                                                	echo -e "$COL_RED [!] $PLUGINKEY (version: $VERSION) is marked incompatible with $PRODUCT $PRODUCTVERSION and is an archived plugin! $DCRESULT("$COUNTPLUGINS"/"$TOTALPLUGINS")"$COL_RESET
						fi
					fi
			fi
		fi
	done
	rm -f /tmp/application-properties.xml
	if [ $WIKIOUTPUT = true ]; then
		echo "|"$WGREEN"(/) Green - Plugin is compatible"$WCX"|||||"
		echo "|"$WYELLOW"(?) Yellow (Unknown) - Plugin has no marketplace listing (this does not mean it's incompatible, you may need to verify manually)"$WCX"|||||"
		echo "|"$WRED"(x) Red - Plugin is incompatible or requires attention for another reason (archived/no martketplace listing for specific version)"$WCX"|||||"
                echo "|Created with http://tiny.cc/5bc4zx|||||"
	else
		if [ "$TOTALPLUGINS" = "1" ]; then 
			echo -e $COL_YELLOW"There are no user installed plugins."$COL_RESET
		fi
		echo -e $COL_BLUE"Compatibility check finished $COL_RESET"
		echo -e $COL_GREEN"\tGreen - Plugin is compatible\n\t"$COL_YELLOW"Yellow - Plugin has no marketplace listing (this does not mean it's incompatible, you may need to verify manually)\n\t"$COL_RED"Red - Plugin is incompatible or requires attention for another reason (archived/no marketplace listing for specific version)"$COL_RESET
	fi
	exit 0
else
	echo -e $COL_RED"Specified input file does not exist (or not a file): "$INPUTXML" - it must be the first parameter!"$COL_RESET
	exit 1
fi
