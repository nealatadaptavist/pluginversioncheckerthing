## Usage
## 
## 1. git clone https://bitbucket.org/peterkoczan/pluginversioncheckerthing
## 2. docker build -t pvct pluginversioncheckerthing
## 3. docker run -ti -v /:/mnt:ro pvct /bin/bash 
##
## You can access your local filesystem through /mnt inside the container (for example /home/user/application.xml will be under /mnt/home/user/application.xml)
##     To run the plugin check on that file, execute:
##                    /opt/pvct/pluginversioncheckerthing/pvct.sh /mnt/home/user/application.xml
##     or
##                    pvct /mnt/home/user/application.xml

FROM ubuntu
MAINTAINER pkoczan@atlassian.com
# Install prerequisites
RUN apt-get update && apt-get install -y software-properties-common python-software-properties curl grep jq xmlstarlet git

# get the tool
RUN mkdir -p /opt/pvct && cd /opt/pvct && git clone https://bitbucket.org/peterkoczan/pluginversioncheckerthing
# Create symlink
RUN ln -s /opt/pvct/pluginversioncheckerthing/pvct.sh /bin/pvct
RUN echo "cd /opt/pvct/pluginversioncheckerthing/ && git pull" >>/root/.bashrc
RUN echo "pvct" >>/root/.bashrc
RUN echo "mkdir -p /var/tmp/pvct" >>/root/.bashrc
RUN echo "cp -rf /opt/pvct/pluginversioncheckerthing/.cache/* /var/tmp/pvct/" >>/root/.bashrc
RUN echo "echo \"Tip: You can access your local filesystem under /mnt. So if you want to scan the file /home/user/application.xml, use: pvct /mnt/home/user/application.xml\"" >>/root/.bashrc
