## What is this? ##
Most Atlassian applications (JIRA, Confluence, Stash, FishEye/Crucible, Bamboo) have the Support Tools Plugin (https://marketplace.atlassian.com/plugins/com.atlassian.support.stp) installed. This way, under **Administration > Atlassian Support Tools > Support Zip** you can generate a package with information about the instance. This contains an **application.xml** file that has the list of installed plugins and their versions. 

This tool processes this list from this file and calls the Marketplace API to query the compatibility of the specific version of the plugin to the version of the application instance it is installed in.

## Prerequisites ##

* jq - http://stedolan.github.io/jq/

* xmlstarlet - http://xmlstar.sourceforge.net/download.php

* curl - http://curl.haxx.se/download.html

* egrep - https://www.topbug.net/blog/2013/04/14/install-and-use-gnu-command-line-tools-in-mac-os-x/

(or use your OS's version of the same packages, the commands need to work that's all)

### For Ubuntu ###
apt-get install jq xmlstarlet curl grep

### For Mac ###
Use homebrew to install the required packages (http://brew.sh/):

* brew install jq xmlstarlet curl

--OR--

Download curl from http://curl.haxx.se/download.html

Download xmlstarlet from http://www.finkproject.org/pdb/package.php/xmlstarlet

Download jq from http://stedolan.github.io/jq/download/

Download egrep from https://www.topbug.net/blog/2013/04/14/install-and-use-gnu-command-line-tools-in-mac-os-x/

Having trouble installing all the above tools on Mac, but you do have docker installed and running? Good news for you! Proceed to the docker section below.

### After that ###
1. git clone https://bitbucket.org/peterkoczan/pluginversioncheckerthing
2. cd pluginversioncheckerthing
3. Proceed to Usage section

## Usage ##

Supports: JIRA, Confluence, Stash, FishEye/Crucible, Bamboo

Usage: ./pvct.sh <path-to>/application.xml

* [-i] outputs incompatible plugins only

* [-n] Hide plugins with no Marketplace information

* [-v <version>] will override the product version from application.xml

* [-w] will produce the output in a wiki-markup table format

* [-dc] will perform Data Center compatibility check

TIP: Use the following to get the output of the command to your clipboard:
Linux (install package xclip): pvct.sh application.xml -w | xclip -selection clipboard
MacOS: pvct.sh application.xml -w | pbcopy

sample output:
![pvct.png](https://bitbucket.org/repo/LjAMxB/images/1965157764-pvct.png)

## Using docker ##
There's a Dockerfile in the repository, so if you have cloned it locally, you can build the image and run it, then you have a fully setup environment that can run the script in. Something like this should work, if you have docker.io installed and docker daemon running:

1. git clone https://bitbucket.org/peterkoczan/pluginversioncheckerthing

2. docker build -t pvct pluginversioncheckerthing

3. docker run -ti -v /:/mnt:ro pvct /bin/bash

(later on you only need to run the 3rd command to use the latest version)

Inside the container you can use the command "pvct". The last command also mounts your root (/) directory as /mnt in the container, so you can access your local (host) filesystem from inside the container and run: "pvct /mnt/<your-local-path-to>/application.xml" for example.